# Demo React Native

## Pasos para ejecutar la aplicacion:

### Instalar:
- NodeJs (agregar en variables de entorno)
- NPM 
- Python (agregar en variables de entorno)
- Java (agregar en variables de entorno)
- Android Studio

> **Nota: en Android Studio se debe configurar las siguientes cosas:
- Ir a Settings -> Android SDK
- Quitar check en "Hide Obsolete Package"
- En la pestaña SDK Platforms Seleccionar un SDK Platform
- En la pestaña SDK Tools marcar: Android SDK Build-Tools, Android Emulator y Android SDK Platform-Tools
- Ir a ADV Manager -> Create Virtual Device y seleccionar el tipo de dispositivo y configurarlo

### Ejecutar
- Crear un proyecto en la ruta deseada con el comando npx init 
- En la ruta del projecto hacer npx 

### Para hacer debugger:
- http://localhost:8081/debugger-ui/
- Ctrl + M sobre la pantalla del emulador de Android y seleccionar "Debug"
